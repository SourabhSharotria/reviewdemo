//
//  SplashViewController.swift
//  ClinicWorldDoctor
//
//  Created by CL-macmini53 on 3/8/16.
//  Copyright © 2016 Click-Labs. All rights reserved.
//

import UIKit
import CoreLocation

class SplashViewController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource, CLLocationManagerDelegate {

//    var locationManager = CLLocationManager()
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var collectionView: UICollectionView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.distanceFilter = kCLDistanceFilterNone
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        
//        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.NotDetermined){
//            println("Not Authorised")
//            locationManager.requestWhenInUseAuthorization()
//        }
        
//        if NSUserDefaults.standardUserDefaults().valueForKey(Keys.kSPAccessToken as String) != nil {
//            self.performSelector(#selector(accessTokenLogin), withObject: self, afterDelay: 0.1)
//        }
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        pageControl.numberOfPages = 3
        pageControl.addTarget(self, action: #selector(SplashViewController.changePage(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        //self.navigationController!.navigationBar.hidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController!.navigationBar.hidden = true
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
        collectionView.reloadData()
        //self.updateLocation()
    }
    
    override func viewDidAppear(animated: Bool) {
        
        //        self.navigationController?.navigationBarHidden = true
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Unwind Segue
    
    @IBAction func backFromSignIn(segue:UIStoryboardSegue){
        
    }
    
    @IBAction func doctorSignUpClick(sender: AnyObject) {
        
        DoctorRegistrationData.sharedInstance.isRegisteringDoctor = true
        
        self.performSegueWithIdentifier("Splash-SignUp", sender: self)
    }
    
    @IBAction func labRoomPressed(sender: AnyObject) {
        
        DoctorRegistrationData.sharedInstance.isRegisteringDoctor = false
        
        performSegueWithIdentifier("Splash-SignUp", sender: self)
        
    }
    
    
//    func updateLocation() {
//        locationManager.startUpdatingLocation()
//    }
//    // MARK: - Location manager delegates
//
//    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
//        // 2
//        
//        if status == CLAuthorizationStatus.AuthorizedWhenInUse {
//            // 3
//            locationManager.startUpdatingLocation()
//        }
//    }
//    
//    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
//        
//    }
//    
//    // 5
//    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        if let location:CLLocation = locations.first{
//            
//            NSUserDefaults.standardUserDefaults().setDouble(Double(location.coordinate.latitude), forKey: Keys.kCPLatitude as String)
//            NSUserDefaults.standardUserDefaults().setDouble(Double(location.coordinate.longitude), forKey: Keys.kCPLongitude as String)
//            
//            NSUserDefaults.standardUserDefaults().synchronize()
//            // 6
//            //          mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
//            
//            // 7
//            locationManager.stopUpdatingLocation()
//        }
//    }
    
    func accessTokenLogin() {
    
        var voipToken = ""
        if let tok = NSUserDefaults.standardUserDefaults().valueForKey((Keys.kSPVoipDeviceToken)) as? String{
            
            voipToken = tok
            
        }
        NSUserDefaults.standardUserDefaults().valueForKey(Keys.kSPDeviceToken as String)!
        
        let params = ["deviceToken": NSUserDefaults.standardUserDefaults().valueForKey(Keys.kSPDeviceToken) as! String,
                      "voipDeviceToken": voipToken]
        
        print(params)
    
    }
    
    // MARK: - Collection View Datasource and Delegates
    
    func collectionView(collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize
    {
        
        print(collectionView.frame.width)
        
        let cellSize:CGSize = CGSizeMake(UIScreen.mainScreen().bounds.width, collectionView.frame.height)
        //let cellSize:CGSize = CGSizeMake(collectionView.frame.width, collectionView.frame.height)
        return cellSize
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = 3
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        return cell
    }
    
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * collectionView.frame.size.width
        collectionView.setContentOffset(CGPointMake(x, 0), animated: true)
        let pageNumber = round(collectionView.contentOffset.x / collectionView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }

    // MARK: - Server File Delgate Actions
    
    func callAlertView(title:String, message:String) {
        dispatch_async(dispatch_get_main_queue(), {
            showAlert("", message: message, viewController: self)
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  Keys.swift
//  SecretSpaArtist
//
//  Created by Samar Singla on 5/13/15.
//  Copyright (c) 2015 Clicklabs, Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit

// -------------------------------Structs---------------------------------

let kAppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)

let kInternalServerError:String = "Something went wrong. Please check your connection and try again."
let kInternetConnectionError:String = "You may not have internet connection. Please check your connection and try again."

typealias HeaderNameImageType = (String,String)

struct ColorCodes {
    
    static let lightGrayColor = UIColor.lightGrayColor()
    static let whiteColor = UIColor.whiteColor()
    
    static let placeholderColor = UIColor(red: 255.0/255.0, green: 133.0/255.0, blue: 20.0/255.0, alpha: 1.0)
    static let headerBarColor = UIColor(red: 253.0/255.0, green: 203.0/255.0, blue: 148.0/255.0, alpha: 1.0)
    static let cellBtnColor = UIColor(red: 251.0/255.0, green: 192.0/255.0, blue: 130.0/255.0, alpha: 1.0)
    static let floatingPlaceholderColor = UIColor(red: 255.0/255.0, green: 124.0/255.0, blue: 19.0/255.0, alpha: 1.0)
    static let floatPlaceholderColor = UIColor(red: 255.0/255.0, green: 144.0/255.0, blue: 19.0/255.0, alpha: 1.0)
    static let greenholderColor = UIColor(red: 0.0/255.0, green: 195.0/255.0, blue: 172.0/255.0, alpha: 1.0)
    
    static let buttonTextColor = UIColor.blackColor()
    static let backGroundView = UIColor(red: 0.9020, green:0.8980, blue: 0.9020, alpha: 1.0)
    static let backGround = UIColor(red: 239.0/255.0, green: 239.0/255.0, blue: 244.0/255.0, alpha: 1.0)
    static let buttonTextHighlightColor = UIColor(red: 255.0/255.0, green: 102.0/255.0, blue: 3.0/255.0, alpha: 1.0)
    static let activityIndicatorBgColor = UIColor.whiteColor()
    
    static let alertBgColor = UIColor.whiteColor()
    static let menuBgView = UIColor(red: 62.0/255.0, green: 57.0/255.0, blue: 54.0/255.0, alpha: 1.0)
}

struct PathToDocumentDirectory {
    static let Documents = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] 
    static let Tmp = NSTemporaryDirectory()
}

struct HTTPStatusCode {
    static let success = 200
    static let created = 201
    static let badRequest = 400
    static let unauthorized = 401
    static let userNotFound = 404
    static let unsupportedError = 415
    static let internalServerError = 500
    static let paymentError = 600
    static let internalError = 409
}

struct NotificationKeys {
//    static let imageSelectedNotification = "imageSelectedNotification"
}
 
struct FontName {
    static let AvenirMediumFont = "Avenir-Medium"
    static let AvenirRomanFont = "Avenir-Roman"
    static let RobotoMediumFont = "Roboto-Medium"
}

struct OrderStatus {
    static let timeOut = "TIMEOUT"
    static let orderPlaced = "ORDER_PLACED"
    static let quoteAccepted = "QUOTE_ACCEPTED"
    static let reachedPickUpPoint = "REACHED_PICKUP_POINT"
    static let pickedUp = "PICKED_UP"
    static let orderDelivered = "ORDER_DELIVERED"
    static let refund = "REFUND"
    
    static let driverAssigned = "DRIVER_ASSIGNED"
    static let driverAccepted = "DRIVER_ACCEPTED"
    static let reachedDeliveryPoint = "REACHED_DELIVERY_POINT"
    
    static let orderCancelled = "CANCELLED"
    static let orderAccepted = "ACCEPTED"
    
}

struct ProfileStatus {
    
    static let kVerification:String = "VERIFICATION"
    static let kBasicInfo:String = "BASIC_INFO"
    static let kProfileDetail:String = "PROFILE_DETAILS"
    static let kPending:String = "PENDING"
    static let kApproved:String = "APPROVED"
    static let kNotRegistered:String = "NOT_REGISTERD"
    static let kDeclined:String = "DECLINED"
}

struct Keys {

    static let kDeviceName:String = "IOS"
    static let kDeviceType:String = "IOS"
    static let kAppVersion:String = "100"
    
    static let kAppstorelink:String = ""
    
    static let kGoogleMapAPIKey:String = "AIzaSyBhlJyfIXhN4B4i5XeFTpyb0UTP-aU7mzg"
    static let kGoogleMapBrowserKey:String = "AIzaSyA0hvUgqR_zDYYWDZQBeTeqw-i8jjxsaug"
    static let kGoogleMapServerKey:String = "AIzaSyBuWDjFDVJaOzII_yQYSGeD_xg-2CqSKbY"
    static let kSPDeviceToken:String = "deviceToken"
    static let kSPVoipDeviceToken:String = "voipdeviceToken"
    static let kSPAccessToken:String = "accessToken"
    
    static let kCPLatitude:String = "Latitude"
    static let kCPLongitude:String = "Longitude"
    
}

enum AppointmentStates:String {
    case ACCEPTED
    case CANCELED
    case COMPLETED
    case RESCHEDULE
    case DECLINED
    case PENDING
    case NO_SHOW
}

enum NotificationType:String {
    case NEW_APPOINTMENT
    case PATIENT_ACCEPTED
    case DOCTOR_REFERRED_PATIENT
    case PATIENT_COMPLETED
    case NEW_MESSAGE
    case PATIENT_RESCHEDULED_ACCEPTED
    case PATIENT_CANCELLED
    case PATIENT_DECLINED
    case PATIENT_DECLINED_REFER
    
    case DOCTOR_REFERRED_DOCTOR
    case CMM_REQUEST
}

enum SharingType:String {
    case EMAIL
    case SMS
}

enum AppType:String {
    case DOCTOR
    case PATIENT
}

struct CalenderStates {
    
    static let kStateFull:String = "FULL"
    static let kStatePartial:String = "PARTIAL"
    static let kStateAvailable:String = "AVAILABLE"
    static let kStateBlocked:String = "BLOCKED"
}

struct CalenderStatesColors {
    
    static let whiteColor = UIColor.whiteColor()
    
    static let kStateFullColor = UIColor(red: 254.0/255.0, green: 131.0/255.0, blue: 18.0/255.0, alpha: 1.0)
    static let kStatePartialColor = UIColor(red: 253.0/255.0, green: 196.0/255.0, blue: 137.0/255.0, alpha: 1.0)
    static let kStateAvailableColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    static let kStateBlockedColor = UIColor(red: 131.0/255.0, green: 131.0/255.0, blue: 131.0/255.0, alpha: 1.0)
}

struct URL {
    
    //static let kBaseUrl:NSString =  "http://54.234.235.159:8888/api/v1/"
    static let kBaseUrl:NSString = "http://clinicworld.clicklabs.in:8080/api/v1/" // dev
//    static let kBaseUrl:NSString = "http://clinicworld.clicklabs.in:8081/api/v1/" // live
}
 
func println(object: Any) {
    Swift.print(object)
}

struct FontSpacing {
    static let noFontSpacing:Float = 0
    static let headerFontSpacing:Float = 1.5
}

struct FontSize {
    static let upcomingButtonFontSize:CGFloat = 17
    
    static let loginButtonFontSize:CGFloat = 28
    static let buttonFontSize:CGFloat = 20
    static let calenderButtonSize:CGFloat = 14
    
    static let upcomingFontSize:CGFloat = 80
}

func adjustHeight(value:CGFloat) -> CGFloat {
    
    return (UIScreen.mainScreen().bounds.height*value)/568;
}

enum SearchType:String {
    case LOCATION
    case NAME
}

//
//  SignInViewController.swift
//  ClinicWorldDoctor
//
//  Created by CL-macmini17 on 04/04/16.
//  Copyright © 2016 Click-Labs. All rights reserved.
//

import UIKit
import CoreLocation

class SignInViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        pageControl.numberOfPages = 3
        
        if NSUserDefaults.standardUserDefaults().valueForKey(Keys.kSPAccessToken as String) != nil {
            self.performSelector(#selector(accessTokenLogin), withObject: self, afterDelay: 0.1)
        }
        
        getLocationAccess()
        self.updateLocation()
        
        
    }
    override func viewWillAppear(animated: Bool) {
        
        DoctorRegistrationData.sharedInstance.profileImageOriginal = nil
        DoctorRegistrationData.sharedInstance.isAdmin = false
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController!.navigationBar.hidden = true
        
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
    }
    
    /*
    override func viewDidAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController!.navigationBar.hidden = false
        
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
    }
    */
    
    @IBAction func forgotPasswordClicked(sender: AnyObject) {
        self.performSegueWithIdentifier("SignIn-ForgotPassword", sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func getLocationAccess() {
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.NotDetermined){
            println("Not Authorised")
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func updateLocation() {
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        // 2
        
        if status == CLAuthorizationStatus.AuthorizedWhenInUse {
            // 3
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location:CLLocation = locations.first{
            
            NSUserDefaults.standardUserDefaults().setDouble(Double(location.coordinate.latitude), forKey: Keys.kCPLatitude as String)
            NSUserDefaults.standardUserDefaults().setDouble(Double(location.coordinate.longitude), forKey: Keys.kCPLongitude as String)
            
            NSUserDefaults.standardUserDefaults().synchronize()
            // 6
            //          mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            
            // 7
            locationManager.stopUpdatingLocation()
        }
    }

    func accessTokenLogin() {
        
        var voipToken = ""
        if let tok = NSUserDefaults.standardUserDefaults().valueForKey((Keys.kSPVoipDeviceToken)) as? String{
            
            voipToken = tok
        }
        
        NSUserDefaults.standardUserDefaults().valueForKey(Keys.kSPDeviceToken as String)!
        
        let params = ["deviceToken": NSUserDefaults.standardUserDefaults().valueForKey(Keys.kSPDeviceToken) as! String,
                      "voipDeviceToken": voipToken]
        
        print(params)
    }
    
    @IBAction func signInButtonClicked(sender: AnyObject) {
        
        self.view.endEditing(true)
        
        
        emailTextField.text = truncateSpace(emailTextField.text!)
        if(emailTextField.text!.characters.count == 0){
            showAlert("", message: "Please enter Email.", viewController: self)
        }
        else if((passwordTextField.text!.characters.count==0)){
            showAlert("", message: "Please enter Password.", viewController: self)
        }
        else if(passwordTextField.text!.characters.count <  6){
            showAlert("", message: "Password length must be greater than 6.", viewController: self)
        }
        
        else if isValidEmail(emailTextField.text!)==false && validatePhone(emailTextField.text!) == false{
            
            showAlert("", message: "Email/Phone is not valid", viewController: self)
        }
        else {
            
            //Hit API and Perform Segue
            
            print("Hello")
            
        }
        
    }
    
    // MARK: - Text Field Delegates
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField == passwordTextField){
            textField.resignFirstResponder()

        }else{
            passwordTextField.becomeFirstResponder()
        }
     
        return true
    }
    
    // MARK: - Collection View Datasource and Delegates
    
    func collectionView(collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize
    {
        print(collectionView.frame.origin.y)
        
        let cellSize:CGSize = CGSizeMake(collectionView.frame.width, collectionView.frame.height)
        return cellSize
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        return cell
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * collectionView.frame.size.width
        collectionView.setContentOffset(CGPointMake(x, 0), animated: true)
        let pageNumber = round(collectionView.contentOffset.x / collectionView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
   
    // MARK: - Server File Delgate Actions
    
    func callAlertView(title:String, message:String) {
        
        dispatch_async(dispatch_get_main_queue(), {
            showAlert("", message: message, viewController: self)
        })
    }

    // MARK: - Unwind Segue
    @IBAction func backFromSplash(segue:UIStoryboardSegue){
        
        
    }

}

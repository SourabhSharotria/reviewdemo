//
//  AccountVerificationViewController.swift
//  ClinicWorldDoctor
//
//  Created by CL-macmini53 on 3/22/16.
//  Copyright © 2016 Click-Labs. All rights reserved.
//

import UIKit

class AccountVerificationViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var mobileOTPTextField: UITextField!
    @IBOutlet weak var emailOTPTextField: UITextField!
    
    @IBOutlet weak var changeMobileButton: UIButton!
    @IBOutlet weak var mobileOTPButton: UIButton!
    
    @IBOutlet weak var changeEmailButton: UIButton!
    @IBOutlet weak var emailOTPButton: UIButton!
    
    var inputTextField = UITextField()
    var inputMKTextField = UITextField()
    var isMobileChanged = false
    var isEmailChanged = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DoctorRegistrationData.sharedInstance.profileStatus = ProfileStatus.kVerification
        // Do any additional setup after loading the view.
        
    }

    override func viewWillAppear(animated: Bool) {
        //DoctorRegistrationData.sharedInstance.profileImageOriginal = nil
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController!.navigationBar.hidden = false
        
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
        setAttributes()
       
    }
    
    func setAttributes() {
        
        mobileOTPTextField.delegate = self
        emailOTPTextField.delegate = self
        
        if DoctorRegistrationData.sharedInstance.isPhoneVerified == true {
            
            mobileOTPTextField.backgroundColor = ColorCodes.lightGrayColor
            mobileOTPTextField.enabled = false
            
            changeMobileButton.enabled = false
            mobileOTPButton.enabled = false
            
        }
        
        if DoctorRegistrationData.sharedInstance.isEmailVerified == true {
            
            emailOTPTextField.backgroundColor = ColorCodes.lightGrayColor
            emailOTPTextField.enabled = false
            
            changeEmailButton.enabled = false
            emailOTPButton.enabled = false
        }
        
        changeMobileButton.setAttributedTitle(NSAttributedString(string: "Change Mobile Number", attributes: [NSFontAttributeName:UIFont(name: FontName.AvenirMediumFont, size: 11.0)!,NSForegroundColorAttributeName:ColorCodes.placeholderColor,NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]), forState: UIControlState.Normal)
        
        changeEmailButton.setAttributedTitle(NSAttributedString(string: "Change Email ID", attributes: [NSFontAttributeName:UIFont(name: FontName.AvenirMediumFont, size: 11.0)!,NSForegroundColorAttributeName:ColorCodes.placeholderColor,NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]), forState: UIControlState.Normal)
        
        mobileOTPButton.setAttributedTitle(NSAttributedString(string: "Resend OTP", attributes: [NSFontAttributeName:UIFont(name: FontName.AvenirMediumFont, size: 11.0)!,NSForegroundColorAttributeName:ColorCodes.placeholderColor,NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]), forState: UIControlState.Normal)
        
        emailOTPButton.setAttributedTitle(NSAttributedString(string: "Resend OTP", attributes: [NSFontAttributeName:UIFont(name: FontName.AvenirMediumFont, size: 11.0)!,NSForegroundColorAttributeName:ColorCodes.placeholderColor,NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]), forState: UIControlState.Normal)
        
    }
    
    
    @IBAction func changeMobileNumberClick(sender: AnyObject) {
        self.changeData(sender)
    }
    
    @IBAction func OTPClick(sender: AnyObject) {
        
        self.view.endEditing(true)
        var requestType = String()
        if(isMobileChanged){
           requestType = "PHONE_EMAIL_VERIFICATION"
        }else{
            requestType = "FORGET_PASSWORD"
        }
        
        var userType = "DOCTOR"
        
        if DoctorRegistrationData.sharedInstance.isRegisteringDoctor == false {
            userType = "LAB_ROOM"
        }
        
        let postParamDictionary = [
            "phoneNumber":DoctorRegistrationData.sharedInstance.phoneNumberString,
            "userType": userType,
            "requestType": requestType
            //"requestType": "PHONE_EMAIL_VERIFICATION"
            ]
        
        print(postParamDictionary)
    }
    
    @IBAction func changeEmailIDClick(sender: AnyObject) {
        
        self.changeData(sender)
    }
    
    @IBAction func resendOTPClick(sender: AnyObject) {
        
        self.view.endEditing(true)
        var requestType = String()
        if(isEmailChanged){
            requestType = "PHONE_EMAIL_VERIFICATION"
        }else{
            requestType = "FORGET_PASSWORD"
        }
        
        var userType = "DOCTOR"
        
        if DoctorRegistrationData.sharedInstance.isRegisteringDoctor == false {
            userType = "LAB_ROOM"
        }
        
        let postParamDictionary = [
            "email":DoctorRegistrationData.sharedInstance.emailString,
            "userType": userType,
            "requestType": requestType
            //"requestType": "PHONE_EMAIL_VERIFICATION"
            ]
        
        print(postParamDictionary)
    }
    
    func severChangeEmail(email:String) {
        
        self.view.endEditing(true)
        
        var userType = "DOCTOR"
        
        if DoctorRegistrationData.sharedInstance.isRegisteringDoctor == false {
            userType = "LAB_ROOM"
        }
        
        let postParamDictionary = [
            "email":email,
            "userType": userType,
            "requestType": "PHONE_EMAIL_VERIFICATION"
        ]
        
        print(postParamDictionary)
    }
    
    
    func severChangePhoneNumber(phoneNumber:String) {
        
        self.view.endEditing(true)
        
        var userType = "DOCTOR"
        
        if DoctorRegistrationData.sharedInstance.isRegisteringDoctor == false {
            userType = "LAB_ROOM"
        }
        
        let postParamDictionary = [
            "phoneNumber":phoneNumber,
            "userType": userType,
            "requestType": "PHONE_EMAIL_VERIFICATION"
        ]
        
        print(postParamDictionary)
    }
    
    // MARK: - Button click Action
    
    @IBAction func skipClicked(sender: AnyObject) {
        
        print("Skip Clicked")
        
        
        
    }

    
    @IBAction func submitClick(sender: AnyObject) {
       
        self.view.endEditing(true)
        
//        self.performSegueWithIdentifier("AccountVerification-DoctorVerification", sender: self)
        
        if  DoctorRegistrationData.sharedInstance.isPhoneVerified == false && (mobileOTPTextField.text!.stringByReplacingOccurrencesOfString(" ", withString: "")).characters.count == 0 {
            
            showAlert("", message: "Enter OTP sent in your mobile number.", viewController: self)
        }
        else if DoctorRegistrationData.sharedInstance.isEmailVerified == false && (emailOTPTextField.text!.stringByReplacingOccurrencesOfString(" ", withString: "")).characters.count == 0 {
            showAlert("", message: "Enter OTP sent in your email.", viewController: self)
        }
        else if (DoctorRegistrationData.sharedInstance.isPhoneVerified == false && (mobileOTPTextField.text!.stringByReplacingOccurrencesOfString(" ", withString: "")).characters.count < 6){
            showAlert("", message: "Mobile OTP is not valid", viewController: self)
        }
        else if (DoctorRegistrationData.sharedInstance.isEmailVerified == false && (emailOTPTextField.text!.stringByReplacingOccurrencesOfString(" ", withString: "")).characters.count < 6){
            showAlert("", message: "Email OTP is not valid", viewController: self)
        }
        else {
            
            var postParamDictionary = NSDictionary()
            
            if DoctorRegistrationData.sharedInstance.isPhoneVerified == true {
                postParamDictionary = [
                    "email":DoctorRegistrationData.sharedInstance.emailString,
                    "emailOtp":emailOTPTextField.text!
                ]
            }
            else if DoctorRegistrationData.sharedInstance.isEmailVerified == true {
                postParamDictionary = [
                    "phoneNumber":DoctorRegistrationData.sharedInstance.phoneNumberString,
                    "phoneOtp":mobileOTPTextField.text!
                ]
            }
            else {
                postParamDictionary = [
                    "email":DoctorRegistrationData.sharedInstance.emailString,
                    "emailOtp":emailOTPTextField.text!,
                    "phoneNumber":DoctorRegistrationData.sharedInstance.phoneNumberString,
                    "phoneOtp":mobileOTPTextField.text!,
                ]
            }
         
            print(postParamDictionary)
        }
    }
    
    @IBAction func viewTutorialClick(sender: AnyObject) {
    }
    
    // MARK: - Touch Delgates
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func changeData(sender: AnyObject)  {
        let actionSheet:UIAlertController!
        
        if sender.tag == 1 {
            
            let title = NSMutableAttributedString.createAttributeText(ColorCodes.placeholderColor, font: UIFont(name: FontName.AvenirMediumFont, size: 14)!, text:"Change Mobile Number", spacing: 0)
            
            actionSheet = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.Alert)
            actionSheet.setValue(title, forKey: "attributedMessage")
        }
        else{
            
            let title = NSMutableAttributedString.createAttributeText(ColorCodes.placeholderColor, font: UIFont(name: FontName.AvenirMediumFont, size: 14)!, text:"Change email ID", spacing: 0)
            
            actionSheet = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.Alert)
            actionSheet.setValue(title, forKey: "attributedMessage")
            
        }
        
        actionSheet.addTextFieldWithConfigurationHandler { (textField) -> Void in
            
            self.inputTextField = textField
            
            self.inputTextField.borderStyle = UITextBorderStyle.None
            self.inputTextField.layer.borderWidth = 0
            
            if sender.tag == 1 {
                self.inputTextField.tag = 1
                self.inputTextField.delegate = self
                self.inputTextField.leftView = setPaddingView("", hasImage: true)
                textField.keyboardType = UIKeyboardType.NumberPad
            }
            else{
                self.inputTextField.tag = 2
                self.inputTextField.leftView = setPaddingView("", hasImage: true)
                textField.keyboardType = UIKeyboardType.Default
            }
        }
        
        let saveAction = UIAlertAction(title: "Submit", style: UIAlertActionStyle.Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if self.inputTextField.text?.stringByReplacingOccurrencesOfString(" ", withString: "") != "" {
                if sender.tag == 1 {
                    
                    var phoneNumber:NSString = (self.inputTextField.text)!
                    phoneNumber = (((phoneNumber.stringByReplacingOccurrencesOfString("-", withString: "")).stringByReplacingOccurrencesOfString("(", withString: "")).stringByReplacingOccurrencesOfString(")", withString: "")).stringByReplacingOccurrencesOfString(" ", withString: "")
                    
                    //DoctorRegistrationData.sharedInstance.phoneNumberString = phoneNumber as String
                    self.severChangePhoneNumber(phoneNumber as String)
                }
                else{
                    if isValidEmail(self.inputTextField.text!) {
                        //DoctorRegistrationData.sharedInstance.emailString = self.inputTextField.text!
                        let email:NSString = (self.inputTextField.text)!
                        self.severChangeEmail(email as String)
                    }
                    else {}
                    
                }
            }
            else{
                
                showAlert("", message: "Please provide detail.", viewController: self)
                
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        actionSheet.addAction(saveAction)
        actionSheet.addAction(cancelAction)
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    // MARK: Textfield Delegate
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == self.mobileOTPTextField || textField == self.emailOTPTextField){
            let maxLength = 6
            let currentString: NSString = textField.text!
            let newString: NSString =
                currentString.stringByReplacingCharactersInRange(range, withString: string)
            if(textField == mobileOTPTextField){
                //mobileOTPButton.setTitleColor(UIColor.greenColor(), forState: .Normal)
                if(newString.length > 0){
                    mobileOTPButton.setAttributedTitle(NSAttributedString(string: "Resend OTP", attributes: [NSFontAttributeName:UIFont(name: FontName.AvenirMediumFont, size: 11.0)!,NSForegroundColorAttributeName:ColorCodes.greenholderColor,NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]), forState: UIControlState.Normal)
                    changeMobileButton.setAttributedTitle(NSAttributedString(string: "Change Mobile Number", attributes: [NSFontAttributeName:UIFont(name: FontName.AvenirMediumFont, size: 11.0)!,NSForegroundColorAttributeName:ColorCodes.greenholderColor,NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]), forState: UIControlState.Normal)
                    
                }
                else{
                    mobileOTPButton.setAttributedTitle(NSAttributedString(string: "Resend OTP", attributes: [NSFontAttributeName:UIFont(name: FontName.AvenirMediumFont, size: 11.0)!,NSForegroundColorAttributeName:ColorCodes.placeholderColor,NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]), forState: UIControlState.Normal)
                    changeMobileButton.setAttributedTitle(NSAttributedString(string: "Change Mobile Number", attributes: [NSFontAttributeName:UIFont(name: FontName.AvenirMediumFont, size: 11.0)!,NSForegroundColorAttributeName:ColorCodes.placeholderColor,NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]), forState: UIControlState.Normal)
                }
            }else{
                if(newString.length > 0){
                    
                    changeEmailButton.setAttributedTitle(NSAttributedString(string: "Change Email ID", attributes: [NSFontAttributeName:UIFont(name: FontName.AvenirMediumFont, size: 11.0)!,NSForegroundColorAttributeName:ColorCodes.greenholderColor,NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]), forState: UIControlState.Normal)
                    emailOTPButton.setAttributedTitle(NSAttributedString(string: "Resend OTP", attributes: [NSFontAttributeName:UIFont(name: FontName.AvenirMediumFont, size: 11.0)!,NSForegroundColorAttributeName:ColorCodes.greenholderColor,NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]), forState: UIControlState.Normal)
                    
                }else{
                    
                    changeEmailButton.setAttributedTitle(NSAttributedString(string: "Change Email ID", attributes: [NSFontAttributeName:UIFont(name: FontName.AvenirMediumFont, size: 11.0)!,NSForegroundColorAttributeName:ColorCodes.placeholderColor,NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]), forState: UIControlState.Normal)
                    emailOTPButton.setAttributedTitle(NSAttributedString(string: "Resend OTP", attributes: [NSFontAttributeName:UIFont(name: FontName.AvenirMediumFont, size: 11.0)!,NSForegroundColorAttributeName:ColorCodes.placeholderColor,NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]), forState: UIControlState.Normal)
                }
            }
            return newString.length <= maxLength
        }
        
        if textField == self.inputTextField{
            
            if textField.tag == 1 {
                
                let phoneString:NSString = "\(textField.text!)\(string)"
                if (range.length == 1) {
                    // Delete button was hit.. so tell the method to delete the last char.
                    self.inputTextField.text = formatPhoneNumber(phoneString, deleteLastChar: true) as String
                } else {
                    self.inputTextField.text = formatPhoneNumber(phoneString, deleteLastChar: false) as String
                }
                
                return false
            }
            
            
        }
        
        return true
    }
    
    // MARK: - Prepare For Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "AccountVerification-DoctorVerification"){
            print("Hello")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Server File Delgate Actions
    
    func callAlertView(title:String, message:String) {
        dispatch_async(dispatch_get_main_queue(), {
            showAlert("", message: message, viewController: self)
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
